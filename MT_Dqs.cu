#include <stdio.h>
#include<ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cuda.h>
#include<time.h>
#include<sys/time.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions
#include <helper_functions.h>
#include <helper_cuda.h>

#define THREADS_PER_BLOCK 512


void print(float *list,int length)
{
        for(int i = 0; i < length; i++)
        {
                printf("%f\t",list[i]);
        }
        printf("\n\n\n");
}

__global__ void single_quicksort(float *data, int left, int right)
{
	 int nleft, nright;
	 float *lptr = data + left;
	 float *rptr = data + right;
	 float pivot = data[left + (right - left)/2];
	 cudaStream_t s1,s2;

	 while(lptr <= rptr)
	 {
		int lval = *lptr;
		int rval = *rptr;

		while(lval < pivot)
		{	
			lptr++;
			lval = *lptr;
		}

		while(rval > pivot)
		{
			rptr--;
			rval=*rptr;
		}

		if(lptr<=rptr)
		{	
			*lptr++ = rval;
			*rptr-- = lval;
		}
	 }

	 nright = rptr - data;
         nleft  = lptr - data;

	if(left < nright) 
	{
		cudaStreamCreateWithFlags( &s1, cudaStreamNonBlocking );
		single_quicksort<<<1 ,1, 0, s1 >>>(data, left, nright);
		cudaStreamDestroy(s1);
	}

	if(nleft < right) 
	{
		cudaStreamCreateWithFlags( &s2, cudaStreamNonBlocking );
		single_quicksort<<<1, 1, 0, s2>>>(data, nleft, right);
		cudaStreamDestroy(s2);
	}
}

__global__ void partition(float *input, float *output,float pivot, int left, int right, int leq[], 
				int gt[], int *leq_val, int *gt_val, int num_blocks)
{
	__shared__ float block_Input[THREADS_PER_BLOCK];
  	__syncthreads();
  	
	int idx = left + blockIdx.x * blockDim.x + threadIdx.x;
  	
	__shared__ int left_block, right_block;
  	__shared__ int left_offset, right_offset;


  	if(threadIdx.x == 0)
  	{
    		leq[blockIdx.x] = 0;
    		gt[blockIdx.x] = 0;
  	}	

  	__syncthreads();

  	if(idx <= (right - 1))
  	{
    		block_Input[threadIdx.x] = input[idx];

    		if(block_Input[threadIdx.x] <= pivot)
    		{
        		atomicAdd( &(leq[blockIdx.x]), 1);
    		}
    		else 
    		{
        		atomicAdd( &(gt[blockIdx.x]), 1);
    		}  
  	}
  	__syncthreads();
  
 	if (threadIdx.x == 0)
 	{
      		left_block = leq[blockIdx.x];
      		left_offset = left + atomicAdd(leq_val, left_block);
 	}
 	if (threadIdx.x == 1)
	{
      		right_block = gt[blockIdx.x];
      		right_offset = right - atomicAdd(gt_val, right_block);
  	}
  	__syncthreads();

  	if(threadIdx.x == 0)
 	{
    		int m = 0;
    		int n = 0;
    		for(int j = 0; j < THREADS_PER_BLOCK; j++)
    		{
      			int chk = left + blockIdx.x*THREADS_PER_BLOCK + j;
      			if(chk <= (right - 1) )
			{
				if(block_Input[j] <= pivot)
        			{
	  				output[left_offset + m] = block_Input[j];
	  				++m;
				} 
        			else 
        			{
	  				output[right_offset - n] = block_Input[j];
	  				++n;
				}
      			}
    		}
  	}

  	__syncthreads();
  	return;
}

__global__ void  multi_quicksort(float *input, int left, int right, int length, int depth, int max_depth)
{
  	if((right - left) >= 1)
  	{
    		
 		if (depth >= max_depth)
    		{
        		single_quicksort<<<1,1>>>(input, left, right);
        		return;
    		}

		cudaStream_t s1,s2;
    
    		float pivot = input[right];

    		int num_blocks = (right - left) / THREADS_PER_BLOCK + 1;
    		int *leq_val = (int *) malloc(sizeof(int)); 
    		int *gt_val = (int *)malloc(sizeof(int));
    		*leq_val = 0;
    		*gt_val = 0; 
       
    		float *output = (float*)malloc(sizeof(float) * (length));
    		for(int i = 0; i < length; i++)
    		{
        		output[i] = input[i];
    		}

    		int *leq = (int*)malloc(sizeof(int)*num_blocks);
    		int *gt = (int*)malloc(sizeof(int)*num_blocks);
 		
    		for(int i = 0; i < num_blocks; i++)
    		{
        		leq[i] = 0;
        		gt[i] = 0;
    		}    

    		partition<<<num_blocks, THREADS_PER_BLOCK>>>(input, output, pivot, left, right, leq, gt, leq_val, gt_val, num_blocks);
    		
		cudaDeviceSynchronize();

    		int position_offset = left;
    		for(int i =0; i < num_blocks; i++)
    		{
        		position_offset = position_offset + leq[i];
    		}
    
    		output[position_offset] = pivot;
 
    		for(int i = left; i <= right; i++)
    		{
			input[i] = output[i];
    		}

 		free(output);
    		free(leq);
    		free(gt);
    		free(leq_val);
    		free(gt_val);

    		if(position_offset-1 >= left)
    		{
			cudaStreamCreateWithFlags(&s1,cudaStreamNonBlocking);
			multi_quicksort<<<1,1,0,s1>>>(input, left, position_offset-1, length, depth + 1, max_depth);
    		}
    		if(position_offset+1 <= right)
    		{
			cudaStreamCreateWithFlags(&s2,cudaStreamNonBlocking);
        		multi_quicksort<<<1,1,0,s2>>>(input, position_offset+1, right, length, depth + 1, max_depth);
    		}
    
    		cudaDeviceSynchronize();
  	}
  	return;
}


int main(void)
{       
        int device_count = 0;
	int device = -1;

        checkCudaErrors(cudaGetDeviceCount(&device_count));

        for (int i = 0 ; i < device_count ; ++i)
        {
            cudaDeviceProp properties;
            checkCudaErrors(cudaGetDeviceProperties(&properties, i));

            if (properties.major > 3 || (properties.major == 3 && properties.minor >= 5))
            {
                device = i;
                printf("\nRunning on GPU: %d ", i);
            	printf("\nProperties Name: %s\n", properties.name);
                break;
            }
	    printf("GPU: %d(%s) does not support CUDA Dynamic Parallelism\n", i, properties.name);
        }

	if (device == -1)
    	{
        	printf("Dynamic Quicksort requires GPU with compute architecture of SM 3.5 or higher.  Exiting...\n");
        	exit(1);
    	}	

        cudaSetDevice(device);

	float time;
	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	
	int n=0;
        printf("Enter number of items in the list\n");
        scanf("%d",&n);
	
	int max_depth = 4;
	if(n>100000)
	{
		max_depth = 1;
	}

	checkCudaErrors(cudaDeviceSetLimit(cudaLimitDevRuntimeSyncDepth, max_depth));	
        size_t size = n * sizeof(float);
	
        float *host_list;
        host_list = (float *)malloc(size);

	float *dev_list;
	checkCudaErrors(cudaMalloc((void **)&dev_list, size));
        
        for(int j=0;j<n;j++)
        {
                host_list[j]=rand()%n;
        }

        printf("Unsorted list is : \n");
        print(host_list,n);

	checkCudaErrors(cudaMemcpy(dev_list,host_list, size,cudaMemcpyHostToDevice ));
	
	cudaEventRecord(start,0);
	multi_quicksort<<<1,1>>>(dev_list,0,n-1, n, 0, max_depth);
        cudaDeviceSynchronize();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	
	checkCudaErrors(cudaMemcpy(host_list, dev_list, size, cudaMemcpyDeviceToHost));
        
        printf("\n\n\n");
        printf("Sorted List is : \n");
        print(host_list,n);

        printf("Time to generate:  %6.3f ms \n", time);

        //Cleanup
    	free(host_list);
	cudaFree(dev_list);
}


