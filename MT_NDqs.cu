#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cuda.h>
#include<time.h>
#include<sys/time.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions
#include <helper_functions.h>
#include <helper_cuda.h>

#define THREADS_PER_BLOCK 512

void print(float *list,int length)
{
        for(int i = 0; i < length; i++)
        {
                printf("%f\t",list[i]);
        }
        printf("\n\n\n");
}

__global__ void partition(float *input, float *output, float pivot, int left, int right, int leq[], int gt[], int *leq_val, int *gt_val, int num_blocks)
{
  	__shared__ float block_input[THREADS_PER_BLOCK];
  	__syncthreads();
  
	int idx = left + blockIdx.x * blockDim.x + threadIdx.x;
  	__shared__ int left_block, right_block;
  	__shared__ int left_offset, right_offset;

  	if(threadIdx.x == 0)
  	{
    		leq[blockIdx.x] = 0;
    		gt[blockIdx.x] = 0;
  	}
  	__syncthreads();

  	if(idx <= (right - 1))
	{
    		block_input[threadIdx.x] = input[idx];

    		if( block_input[threadIdx.x] <= pivot )
		{
        		atomicAdd( &(leq[blockIdx.x]), 1);
    		}
		else
		{
        		atomicAdd( &(gt[blockIdx.x]), 1);
    		}
    
  	}
  	__syncthreads();

  	if (threadIdx.x == 0)
	{
      		left_block = leq[blockIdx.x];
      		left_offset = left + atomicAdd(leq_val, left_block);
  	}
  	if (threadIdx.x == 1)
	{
      		right_block = gt[blockIdx.x];
      		right_offset = right - atomicAdd(gt_val, right_block);
  	}
 	__syncthreads();

  	if(threadIdx.x == 0)
	{
    		int m = 0;
    		int n = 0;
    		for(int j = 0; j < THREADS_PER_BLOCK; j++)
		{
      			int chk = left + blockIdx.x * blockDim.x + j;
      			if(chk <= (right - 1))
			{
				if(block_input[j] <= pivot)
				{
	  				output[left_offset + m] = block_input[j];
	  				++m;
				} 
				else
 				{
	  				output[right_offset - n] = block_input[j];
	  				++n;
				}
      			}	
    		}
  	}

  	__syncthreads();	
  	return;
}

void quicksort(float input[], int left, int right, int length)
{
  	if((right - left) >= 1)
	{
    		float pivot = input[right];
    		int num_blocks = (right - left) / THREADS_PER_BLOCK + 1;
		
		float *dev_ls;
    		float *dev_ls2;
    		
    		int *host_leq;
    		host_leq = (int *)malloc(sizeof(int)*num_blocks);
    		for(int k =0; k <num_blocks; k++)
    		{
        		host_leq[k] = 0;
    		}
 
    		int * dev_leq, * dev_gt, *dev_leq_val, *dev_gt_val;
    		int size = sizeof(float);
    
		cudaMalloc(&(dev_ls), size*length);
    		cudaMalloc(&(dev_ls2), size*length);
    		cudaMalloc(&(dev_leq), sizeof(int)*num_blocks);
    		cudaMalloc(&(dev_gt), sizeof(int)*num_blocks);
    		cudaMalloc(&dev_leq_val, sizeof(int));
    		cudaMemset(dev_leq_val, 0, sizeof(int));
		cudaMalloc(&dev_gt_val, sizeof(int));
    		cudaMemset(dev_gt_val, 0, sizeof(int));
    
    		cudaMemcpy(dev_ls, input, size*length, cudaMemcpyHostToDevice);
    		cudaMemcpy(dev_ls2, input, size*length, cudaMemcpyHostToDevice);

    		partition<<<num_blocks, THREADS_PER_BLOCK>>>(dev_ls, dev_ls2, pivot, left, right, dev_leq, dev_gt, dev_leq_val, dev_gt_val, num_blocks);
    		cudaDeviceSynchronize();
    
		checkCudaErrors(cudaMemcpy(host_leq,dev_leq,num_blocks*sizeof(int), cudaMemcpyDeviceToHost));

    		int position_offset = left;
    		for(int i =0; i < num_blocks; i++)
    		{
        		position_offset = position_offset + host_leq[i];
    		} 

    		cudaMemcpy(input, dev_ls2, size*length, cudaMemcpyDeviceToHost);
 
    		input[position_offset] = pivot;
  
		cudaFree(dev_ls);
    		cudaFree(dev_ls2);
    		cudaFree(dev_leq);
    		cudaFree(dev_gt);
		cudaFree(dev_gt_val);
		cudaFree(dev_leq_val);
		
		free(host_leq);

    		if(position_offset-1 >= left)
		{
			quicksort(input, left, position_offset-1, length);
		}
    		if(position_offset+1 <= right)
		{
      			quicksort(input, position_offset+1, right, length);
		}
  	}
  	return;
}


int main(void)
{       
        int device_count = 0;
	int  device = -1;

        checkCudaErrors(cudaGetDeviceCount(&device_count));

        for (int i = 0 ; i < device_count ; ++i)
        {
            cudaDeviceProp properties;
            checkCudaErrors(cudaGetDeviceProperties(&properties, i));

            if (properties.major > 3 || (properties.major == 3 && properties.minor >= 5))
            {
                device = i;
                printf("\nRunning on GPU: %d ", i);
            	printf("\nProperties Name: %s\n", properties.name);
                break;
            }

	    printf("GPU: %d(%s) does not support CUDA Atomic Operations\n", i, properties.name);
         }

	if (device == -1)
    	{
        	printf("Atomic Operations requires GPU devices with compute architecture of SM 3.5 or higher.  Exiting...\n");
        	exit(1);
    	}	

        cudaSetDevice(device);

	struct timeval start, end;
	long seconds, useconds, mtime;

	int n=0;
        printf("Enter number of items in the list\n");
        scanf("%d",&n);
	
        size_t size = n * sizeof(float);

        float *host_list;
        host_list = (float *)malloc(size);
      
        for(int j=0;j<n;j++)
        {
                host_list[j]=rand()%n;
        }       


        printf("Unsorted list is : \n");
        print(host_list,n);
	
	gettimeofday(&start,NULL);
        quicksort(host_list, 0, n - 1, n);
	gettimeofday(&end,NULL);
	
        printf("\n\n\n");
        printf("Sorted List is : \n");
        print(host_list,n);
        
        seconds = end.tv_sec - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds)*1000  + useconds/1000.0);
	printf("\nTime Elapsed: %ld ms\n", mtime);

        //Cleanup
    	free(host_list);
}


