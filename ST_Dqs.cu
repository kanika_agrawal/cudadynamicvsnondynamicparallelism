#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <cstdio>
#include <iostream>
#include<sys/time.h>
// CUDA runtime
#include <cuda_runtime.h>

// Helper functions
#include <helper_functions.h>
#include <helper_cuda.h>


__global__ void quicksort(int *data, int left, int right)
{
	 int nleft, nright;
	 int *lptr = data + left;
	 int *rptr = data + right;
	 int pivot = data[left + (right - left)/2];
	 cudaStream_t s1,s2;

	 while(lptr <= rptr)
	 {
		int lval = *lptr;
		int rval = *rptr;

		while(lval < pivot)
		{	
			lptr++;
			lval = *lptr;
		}

		while(rval > pivot)
		{
			rptr--;
			rval=*rptr;
		}

		if(lptr<=rptr)
		{	
			*lptr++ = rval;
			*rptr-- = lval;
		}
	 }

	 nright = rptr - data;
         nleft  = lptr - data;

	 if(left < nright) 
	 {
		cudaStreamCreateWithFlags( &s1, cudaStreamNonBlocking );
		quicksort<<<1 ,1, 0, s1 >>>(data, left, nright);
		cudaStreamDestroy(s1);
	 }

	 if(nleft < right) 
	 {
		cudaStreamCreateWithFlags( &s2, cudaStreamNonBlocking );
		quicksort<<<1, 1, 0, s2>>>(data, nleft, right);
		cudaStreamDestroy(s2);
	 } 
}


void print(int *list,int length)
{
	for(int i = 0; i < length; i++)
	{
		printf("%d\t",list[i]);
	}
	printf("\n");
}

int main(void)
{
	int device_count = 0;
	int device = -1;

        checkCudaErrors(cudaGetDeviceCount(&device_count));

        for (int i = 0 ; i < device_count ; ++i)
        {
            cudaDeviceProp properties;
            checkCudaErrors(cudaGetDeviceProperties(&properties, i));

            if (properties.major > 3 || (properties.major == 3 && properties.minor >= 5))
            {
                device = i;
                printf("\nRunning on GPU: %d ", i);
            	printf("\nProperties Name: %s\n", properties.name);
                break;
            }

	    printf("GPU: %d(%s) does not support CUDA Dynamic Parallelism\n", i, properties.name);
         }

	if (device == -1)
    	{
        	printf("Dynamic Quicksort requires GPU devices with compute SM 3.5 or higher.  Exiting...\n");
        	exit(EXIT_SUCCESS);
    	}	

        cudaSetDevice(device);

	float time;
	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

    	//Pointer to host
	int *host_list;
	
	// Pointer to device arrays
	int *dev_list;

	int length=0;

	printf("Enter number of items in the list\n");
	scanf("%d",&length);

	size_t size = length * sizeof(int);

	// Allocate array on host
	host_list = (int *)malloc(size);
	
	// Allocate array on device      
	checkCudaErrors(cudaMalloc((void **)&dev_list, size));

	// Initialize host array and copy it to CUDA device
	for(int j=0;j<length;j++)
	{
		host_list[j]= rand() % length;
	}	
	printf("Unsorted list is : \n");
	//print(host_list,length);

        cudaMemcpy(dev_list, host_list, size, cudaMemcpyHostToDevice);
	
	cudaEventRecord(start, 0);
	
	quicksort<<<1,1>>>(dev_list,0, length-1);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);	
	
	cudaMemcpy(host_list, dev_list, sizeof(int)*length, cudaMemcpyDeviceToHost);
	
	printf("Sorted List is : \n");
 	print(host_list,length);
	printf("Time to generate:  %6.3f ms \n", time);
	
	//Cleanup
    	free(host_list);
    	cudaFree(dev_list);
}
