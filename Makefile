#NVCC is the compiler path
NVCC=/usr/local/cuda-6.0/bin/nvcc

FLAGS=-G -g -O0
COMPUTE_ARCH=-arch=sm_35
INCLUDES=-I/usr/local/cuda-6.0/samples/common/inc
LIBRARY=-lcudadevrt

all: MT_Dqs MT_NDqs ST_Dqs ST_NDqs

MT_Dqs: MT_Dqs.cu
	$(NVCC) $(FLAGS) $(INCLUDES) $(COMPUTE_ARCH) -rdc=true MT_Dqs.cu -o MT_Dqs $(LIBRARY)

MT_NDqs: MT_NDqs.cu
	$(NVCC) $(FLAGS) $(INCLUDES) $(COMPUTE_ARCH) MT_NDqs.cu -o MT_NDqs

ST_Dqs: ST_Dqs.cu
	$(NVCC) $(FLAGS) $(INCLUDES) $(COMPUTE_ARCH) -rdc=true ST_Dqs.cu -o ST_Dqs $(LIBRARY)

ST_NDqs: ST_NDqs.cu
	 $(NVCC) $(FLAGS) $(INCLUDES) $(COMPUTE_ARCH)  ST_NDqs.cu -o ST_NDqs

clean:
	rm MT_Dqs
	rm MT_NDqs
	rm ST_Dqs
	rm ST_NDqs
