#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <cstdio>
#include <iostream>
#include<sys/time.h>
// CUDA runtime
#include <cuda_runtime.h>

// Helper functions
#include <helper_functions.h>
#include <helper_cuda.h>


void print( int *list,int length)
{
        for(int i = 0; i < length; i++)
        {
                printf("%d\t",list[i]);
        }
        printf("\n");
}

__global__ void partition(int *data,int left, int right, int *pivot_location)
{
	int i = left;
        int j = right;
	int temp;
        int pivot = data[i];
        while (i < j)
        {
                while (data[i] <= pivot && i < right)
                {
                        i++;
                }
                while (data[j] > pivot)
                {
                        j--;
                }
                if (i < j)
                {
                        temp = data[i];
                        data[i] = data[j];
                        data[j] = temp;
                }
        }
        temp = data[left];
        data[left] = data[j];
        data[j] = temp;
        *pivot_location = j;
}

void quicksort( int *data, int left,  int right)
{

	if(right > left)
	{	
	 	int *dev_pivot_location;
	 	int pivot_location;
		
		checkCudaErrors(cudaMalloc((void **)&dev_pivot_location, sizeof( int)));	
	
		partition<<<1, 1>>>(data, left, right, dev_pivot_location);
	
		checkCudaErrors(cudaDeviceSynchronize());
		checkCudaErrors(cudaMemcpy(&pivot_location, dev_pivot_location, sizeof(int),cudaMemcpyDeviceToHost));
               
                quicksort(data, left, pivot_location - 1);
                quicksort(data, pivot_location + 1, right);
        }
}

int main(void)
{
	struct timeval start, end;
        long seconds, useconds, mtime;

    	//Pointer to host
	int *host_list;
	
	//Pointer to device arrays
	int *dev_list;

	int length=0;

	printf("Enter number of items in the list\n");
	scanf("%d",&length);

	size_t size = length * sizeof( int);

	// Allocate array on host
	host_list = (int *)malloc(size);
	
	// Allocate array on device      
	checkCudaErrors(cudaMalloc((void **)&dev_list, size));

	// Initialize host array and copy it to CUDA device
	for(int j=0;j<length;j++)
	{
		host_list[j] = rand() % length;
	}	
	printf("Unsorted list is : \n");
	print(host_list,length);

        cudaMemcpy(dev_list, host_list, size, cudaMemcpyHostToDevice);
	
	gettimeofday(&start,NULL);
	quicksort(dev_list,0, length-1);
	gettimeofday(&end,NULL);

	cudaMemcpy(host_list, dev_list, sizeof(unsigned int) * length, cudaMemcpyDeviceToHost);
	
	printf("Sorted List is : \n");
 	print(host_list,length);

	seconds = end.tv_sec - start.tv_sec;
        useconds = end.tv_usec - start.tv_usec;
        mtime = ((seconds)*1000  + useconds/1000.0);
        printf("\nTime Elapsed: %ld ms\n", mtime);
	
	//Cleanup
    	free(host_list);
    	cudaFree(dev_list);
}
